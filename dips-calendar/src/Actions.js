let nextAppointmentId = 0
export const ADD_APPOINTMENT = appointment => ({
    type: 'ADD_APPOINTMENT',
    id: nextAppointmentId++,
    appointment
});
export function addAppointment(appointment) {
    return { type: ADD_APPOINTMENT, appointment }
}
