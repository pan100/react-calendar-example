import React, { Component } from 'react';
import InsertAppointment from './components/InsertAppointment';
import Calendar from './components/Calendar';

import './App.css';
import DateContent from './components/DateContent';

class App extends Component {
  constructor(props) {
    super(props);
    //get from storage
    let storedAppointments = JSON.parse(localStorage.getItem("appointments"));
    if (storedAppointments === null) {
      storedAppointments = [];
    }
    //Stored in strings, must fix date objects
    for (let i = 0; i < storedAppointments.length; i++) {
      storedAppointments[i].startDate = new Date(storedAppointments[i].startDate);
      storedAppointments[i].endDate = new Date(storedAppointments[i].endDate);
    }
    this.state = {
      selectedDate: new Date(),
      appointments: storedAppointments

    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.appointments !== prevState.appointments) {
      localStorage.setItem("appointments", JSON.stringify(this.state.appointments));
    }
  }
  deleteAppointmentCallback(id) {
    let array = [...this.state.appointments];
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) {
        array.splice(i, 1);
      }
    }
    this.setState({ appointments: array });
  }
  changeDateCallback(date) {
    this.setState({
      selectedDate: date
    });
  }
  addAppointment(appointment) {
    this.setState(prevState => ({
      appointments: [...prevState.appointments, appointment]
    }))
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>React Kalender</h1>
        </header>
        <InsertAppointment addAppointmentCallback={this.addAppointment.bind(this)} />
        <Calendar changeDateCallback={this.changeDateCallback.bind(this)} selectedDate={this.state.selectedDate} />
        <DateContent selectedDate={this.state.selectedDate} deleteAppointmentCallback={this.deleteAppointmentCallback.bind(this)} appointments={this.state.appointments.filter((appointment) =>
          (appointment.startDate.getFullYear() === this.state.selectedDate.getFullYear() &&
            appointment.startDate.getMonth() === this.state.selectedDate.getMonth() &&
            appointment.startDate.getDate() === this.state.selectedDate.getDate()))} />
      </div>
    );
  }
}

export default App;
