import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { render, fireEvent, cleanup } from 'react-testing-library'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

afterEach(cleanup);

test('Put an appointment, observe it, delete it and observe that it is gone', () => {
  //krasjer. Det er noe galt i renderen sin lasting. Det kommer ingen feil i browseren i forbindelse med dette.
  //feilen skjer i Calendar-komponenten.
  const component = render(<App />);
  const input = component.getByTestId("appointment-title");
  const button = component.getByTestId("insert-button");
  const appointmentsDiv = component.getByTestId("appointments");
  fireEvent.change(input, { target: { value: "a" } });
  //post the appointment with a click
  fireEvent.click(button);
  //observe it in DateContent
  expect(appointmentsDiv.childNodes.length).toBe(1);
  const appointmentTitle = component.getByTestId("appointment-view-title");
  expect(appointmentTitle.textContent).toBe("a");
  //delete it
  fireEvent.click(component.getByTestId("appointment-delete-button"));
  expect(appointmentsDiv.childNodes.length).toBe(0);

})