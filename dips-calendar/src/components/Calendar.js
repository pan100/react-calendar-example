import React, { Component } from 'react';

import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';

class Calendar extends Component {
    render() {
        return (
            <div id="calendar">
                <h2>Velg dato for å se:</h2>
                <InfiniteCalendar
                    locale={{
                        blank: 'Velg en dato...',
                        headerFormat: 'DD.MM.YYYY',
                        todayLabel: {
                            long: 'I dag',
                        },
                        weekdays: ['Søn', 'Man', 'Tir', 'Ons', 'Tor', 'Fre', 'Lør'],
                        weekStartsOn: 1,
                    }}
                    width={400}
                    height={250}
                    selected={this.props.selectedDate}
                    onSelect={this.props.changeDateCallback}
                    minDate={this.lastWeek}
                />
            </div>
        );
    }
}

export default Calendar;