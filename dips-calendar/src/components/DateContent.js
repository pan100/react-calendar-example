import React, { Component } from 'react';
import format from 'date-fns/format';
import nb from 'date-fns/locale/nb';
import './DateContent.css';


class DateContent extends Component {
    onItemClick(id) {
        this.props.deleteAppointmentCallback(id);
    }
    render() {
        const sortedAppointments = this.props.appointments.sort((a, b) => parseInt(a.startTime.replace(':', '')) - parseInt(b.startTime.replace(':', '')));
        const appointmentView = sortedAppointments.map((appointment) =>
            <div className="appointment" data-testid="appointment" key={appointment.id}><h3>{appointment.startTime} - {appointment.endTime}</h3>
                <p data-testid="appointment-view-title">{appointment.title}</p>
                <button data-testid="appointment-delete-button" onClick={() => this.onItemClick(appointment.id)}>slett</button>
            </div>);
        return (
            <div id="content">
                <h2>Avtaler på <span>{format(
                    this.props.selectedDate,
                    'dd MMM yyyy',
                    { locale: nb }
                )}</span></h2>
                <div data-testid="appointments">{appointmentView}</div>
            </div>
        );
    }
}

export default DateContent;