import React from 'react'
import { cleanup } from 'react-testing-library'
import DateContent from './DateContent';
import TestRenderer from 'react-test-renderer';

afterEach(cleanup)

test('Shows an appointment correctly', () => {
    let today = new Date();
    let appointments = [{
        id: 0,
        title: "Testavtale",
        startDate: today,
        startTime: "12:00",
        endDate: today,
        endTime: "13:00"
    }];
    const component = TestRenderer.create(<DateContent selectedDate={today} appointments={appointments} />)
    expect(component.root.findByType("h3").children).toContain("12:00");
    expect(component.root.findByType("h3").children).toContain("13:00");
    expect(component.root.findByType("p").children).toContain("Testavtale");
})