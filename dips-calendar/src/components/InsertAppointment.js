import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { registerLocale } from 'react-datepicker';
import TimePicker from 'react-time-picker';

import nb from 'date-fns/locale/nb';

import "react-datepicker/dist/react-datepicker.css";

class InsertAppointment extends Component {
    constructor(props) {
        super(props);
        registerLocale('nb', nb);
        let today = new Date();
        let nextId = localStorage.getItem("nextId");
        if (!nextId) {
            nextId = 0;
        }
        this.state = {
            nextId: nextId,
            title: "",
            lastWeek: new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7),
            startDate: today,
            startTime: '12:00',
            endDate: today,
            endTime: '14:00'
        };
        this.changeTitle = event => this.setState({ title: event.target.value });
        this.changeStartDate = startDate => this.startDateHandler(startDate);
        //todo handler with check
        this.changeStartTime = startTime => this.setState({ startTime });
        this.changeEndDate = endDate => this.endDateHandler(endDate);
        //todo handler with check
        //parseInt("14:30".replace(':',''))
        this.changeEndTime = endTime => this.setState({ endTime });
    }
    startDateHandler(startDate) {
        this.setState({ startDate: startDate });
        this.checkEndDateIsAfterStart(startDate);
    }
    endDateHandler(endDate) {
        this.setState({ endDate: endDate });
        this.checkStartDateIsBeforeEnd(endDate);
    }
    checkStartDateIsBeforeEnd(endDateChanged) {
        //if start date is after end date, start date is set to end date
        if (this.state.startDate.getTime() > endDateChanged.getTime()) {
            alert("fratid ble satt til etter tiltid, setter fratid til samme som tiltid");
            this.setState({ startDate: endDateChanged });
        }
    }
    checkEndDateIsAfterStart(startDateChanged) {
        //if end date is before start date, end date is set to start date
        if (this.state.endDate.getTime() < startDateChanged.getTime()) {
            alert("tiltid ble satt til før ettertid, setter tiltid til samme som fratid");
            this.setState({ endDate: startDateChanged });
        }
    }
    submitHandler(event) {
        event.preventDefault();
        this.resetFields();
    }
    InsertAppointmentOnClick() {
        let newAppointment = {
            id: this.state.nextId,
            title: this.state.title,
            startDate: this.state.startDate,
            startTime: this.state.startTime,
            endDate: this.state.endDate,
            endTime: this.state.endTime
        }
        if ((newAppointment.startTime === null) ||
            (newAppointment.endTime === null) ||
            (newAppointment.startTime.length !== 5) ||
            (newAppointment.endTime.length !== 5)) {
            alert("feil: starttid er ikke satt eller feltet inneholder ugyldige verdier.")
        }
        else if ((newAppointment.startDate.getFullYear() === newAppointment.endDate.getFullYear() &&
            newAppointment.startDate.getMonth() === newAppointment.endDate.getMonth() &&
            newAppointment.startDate.getDate() === newAppointment.endDate.getDate()) && (parseInt(newAppointment.startTime.replace(':', '')) > parseInt(newAppointment.endTime.replace(':', '')))) {
            alert("feil: klokkeslett for start er etter klokkeslett for slutt. Endre.")
        }
        else if (newAppointment.title === "") {
            alert("feil: tittel er blank");
        }
        else {
            this.setState({ nextId: this.state.nextId + 1 });
            this.props.addAppointmentCallback(newAppointment);
            localStorage.setItem("nextId", this.state.nextId);
            this.resetFields();
        }
    }
    resetFields() {
        this.setState({ title: "", startDate: new Date(), startTime: "12:00", endDate: new Date(), endTime: "14:00" });
    }
    render() {
        return (
            <div id="insert">
                <h2>Sett inn ny avtale:</h2>
                <form onSubmit={this.submitHandler.bind(this)}>
                    <label>
                        Tittel:
                <input data-testid="appointment-title" value={this.state.title} onChange={this.changeTitle} type="text" name="title" />
                    </label>
                    <br />
                    <label>Fra:
            <DatePicker
                            selected={this.state.startDate}
                            onChange={this.changeStartDate}
                            locale={"nb"}
                            dateFormat="dd.MM.yyyy"
                        />
                        <TimePicker
                            onChange={this.changeStartTime}
                            value={this.state.startTime}
                            locale="no-NO" />
                    </label>
                    <br />
                    <label>Til:
            <DatePicker
                            selected={this.state.endDate}
                            onChange={this.changeEndDate}
                            locale={"nb"}
                            dateFormat="dd.MM.yyyy"
                        />
                        <TimePicker
                            onChange={this.changeEndTime}
                            value={this.state.endTime}
                            locale="no-NO" />
                    </label>
                    <br />
                    <input type="submit" data-testid="insert-button" value="Legg inn" onClick={this.InsertAppointmentOnClick.bind(this)} />
                </form>
            </div>
        );
    }
}

export default InsertAppointment;